#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int arr[20]; //globales array, damit nicht jeder funktion einzeln uebergeben werden muss
int size = sizeof(arr)/sizeof(0); // -//-

void ausgeben();
void Insertionsort();
void Bubblesort();
void Shakersort();

int main() {

srand(time(0)); //zufallsgenerator
int key;


for(int i = 0; i < size; i++) {     //array mit Zufallszahlen von 1-999 befuellen
    arr[i] = rand() % 1000;
}

printf("Unsortiert: \n\n"); //array unsortiert ausgeben
ausgeben();

printf("\n");
printf("\nNach welchem Verfahren moechten sie die Zahlen sortieren?\n\n");
printf("1. Insertionsort\n2. Bubblesort\n3. Shakersort\n");
scanf("%d", &key);


switch(key) {
    case 1: Insertionsort();
            break;

    case 2: Bubblesort();
            break;

    case 3: Shakersort();
            break;
}

printf("Sortiert: \n"); //array sortiert ausgeben
ausgeben();



return 0;
}


void ausgeben() {
    for(int i = 0; i < size; i++) {     //array ausgeben
    printf("%d ", arr[i]);
    }
}

// Sortieralgorithmen

void Insertionsort() {

    for(int i = 1; i < size; i++) {
    int marker = arr[i];
    int j = i-1;

        while((marker < arr[j]) && (j>=0)) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = marker;
    }
}

void Bubblesort() {
    int swap;

    for(int i = 0; i<size; i++) {

        for(int step = 1; step<size-i; step++) {

            if(arr[step] < arr[step-1]) {
                swap = arr[step];
                arr[step] = arr[step-1];
                arr[step-1] = swap;
            }
        }
    }
}

void Shakersort() {
    int swap, cnt;

    for(cnt = 1; cnt < size/2; cnt++) {

        for(int i = size - cnt; i > 0; i--) {
            if(arr[i] < arr[i-1]) {
                swap = arr[i];
                arr[i] = arr[i-1];
                arr[i-1] = swap;
            }
        }

    for(int i = 0 + cnt; i < size; i++) {
        if(arr[i] < arr[i-1]) {
            swap = arr[i];
            arr[i] = arr[i-1];
            arr[i-1] = swap;
        }
    }
}
}
