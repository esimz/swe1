#include <stdlib.h>
#include <stdio.h>

int len(char s[]) {
     int cnt = 0;

    while (s[cnt] != '\0')
        cnt++;

    return cnt;
}

void strcat(char string1[], char string2[]) {
    int len1 = len(string1);
    int len2 = len(string2);

    for(int i = 0; i < len2; i++) {
    string1[(i+len1)] = string2[i];

    }

}

int main() {

char string1[50] = "Hallo";
char string2[] = "Weltverbesserer";

strcat(string1, string2);

printf("%s\n", string1);



return 0;
}
