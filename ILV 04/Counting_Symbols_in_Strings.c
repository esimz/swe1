#include <stdio.h>
#include <stdlib.h>

int strlen(char string1[]) {
    int cnt = 0;

    while(string1[cnt] != '\0') {
        cnt++;
    }
    return cnt;
}

int countNum(char string1[]) {
    int cnt = 0;

    for(int i = 0; string1[i] != '\0'; i++) {
    if(string1[i] >= '0' && string1[i] <= '9') {
        cnt++;


        }

    }
    return cnt;
}

int countLetter(char string1[]) {
    int cnt = 0;

    for(int i = 0; string1[i] != '\0'; i++) {
    if(string1[i] >= 'A' && string1[i] <= 'Z') {
        cnt++;
        }
        else if(string1[i] >= 'a' && string1[i] <= 'z') {
                cnt++;

            }

    }
    return cnt;
}

int countNoAlphaNum(char string1[]) {
    int cnt = 0;
    int len = strlen(string1);

    for(int i = 0; string1[i] != '\0'; i++) {
        if((string1[i] >= 'A' && string1[i] <= 'Z') || (string1[i] >= 'a' && string1[i] <= 'z') || (string1[i] >= '0' && string1[i] <= '9')) {
            cnt++;
        }
    }
    return len - cnt;
}

int main() {

char string1[50] = "28123(@9123SADlwqweasodinwqi&@&17`1238!*231)_!--";


printf("Im string sind %d Zeichen enthalten\n", strlen(string1));
printf("\nIm string sind %d Ziffern enthalten\n", countNum(string1));
printf("Im string sind %d Buchstaben enthalten\n", countLetter(string1));
printf("Im string sind %d nicht Alphanumerische Zeichen enthalten\n", countNoAlphaNum(string1));
return 0;
}
