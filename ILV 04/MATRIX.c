#include <stdio.h>

#define ZEILE 3
#define SPALTE 3

int main() {

int i, j;
int matrix[ZEILE][SPALTE] = {
    {1, 2, 3},
    {4, 5, 6},
    {7, 8, 9}
};

for(i = 0; i < ZEILE; i++) {
    for(j = 0; j < SPALTE; j++) {
        printf("%d ", matrix[i][j]);
    if(j==ZEILE-1) {
        printf("\n");

        }
    }
}


return 0;
}
